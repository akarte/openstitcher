#ifndef PANZOOMVIEW_H
#define PANZOOMVIEW_H

#include <QGraphicsView>
#include <QGraphicsRectItem>

class PanZoomView : public QGraphicsView
{
public:
    PanZoomView(QWidget* parent = NULL);
    ~PanZoomView();
protected:

    //Take over the interaction
    virtual void wheelEvent(QWheelEvent* event);
};

#endif // PANZOOMVIEW_H
