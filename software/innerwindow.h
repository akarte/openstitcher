#ifndef INNERWINDOW_H
#define INNERWINDOW_H

#include <QWidget>
#include <QGraphicsScene>
#include "imagelist.h"
#include "panzoomview.h"

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgproc/types_c.h"
#include "stitching.h"

namespace Ui {
class innerwindow;
}

class InnerWindow : public QWidget
{
    Q_OBJECT

public:
    explicit InnerWindow(QWidget *parent = 0);
    ~InnerWindow();
    /**
     * @brief selectImages opens a dialog to select images to be stitched
     */
    void selectImages();
    /**
     * @brief createPano create a panoramic image and add to the UI component
     */
    void createPano();
    void findFeatures();
    void pairwiseMatching();
    void addImage(Mat mat);
    void exportImage();

    void drawGrid();
    void removeSelectedItemsAtList();

private:
    Ui::innerwindow *ui;
    /**
     * @brief thumbnail_width thumbnail width of image
     */
    int thumbnail_width;
    /**
     * @brief thumbnail_height thumbnail height of image
     */
    int thumbnail_height;
    int grid_size;

    /**
     * @brief il Widget to display current images to be stitched
     */
    ImageList* il;
    /**
     * @brief view Widget that let manipulate an stitched image
     */
    PanZoomView* view;
    /**
     * @brief scene Widget to display panoramic image
     */
    QGraphicsScene* scene;
    Stitching* stitch;
};

#endif // INNERWINDOW_H
